#include "Scene.h"

#include <fstream>
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <utility>
#include <vector>
#include <omp.h>
#include <algorithm>
#include <random>
#include <ctime>
#include <math.h>

#include <iostream>
#include <limits>
#include <string>

#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"
#include "Material.h"
#include "BVHNode.h"
#include "Texture.h"
#include "Rectangle.h"
#include "MetaBall.h"
#include "PDF.h"
#include "ConstantMedium.h"
#include "Alphabet.h"
#include "Creature.h"
#include "Moveable.h"
#include "MoveableList.h"


MoveableList cornell_box(Image &image, int width, int height, double mix) {
  std::vector<Hitable*> list;
  std::vector<Hitable*> lightList;

  double skylight_min_y = 300;
  double skylight_max_y = 600;
  double skylight_min_x = -100;
  double skylight_max_x = 100;
  double skylight_min_z = -100;
  double skylight_max_z = 100;

  // double windowTop = 275;
	// double windowBottom = 75;
	// double windowLeft = -100;
	// double windowRight = 100;
	// double windowFront = 300;
	// double windowBack = 600;


  Material *light = new DiffuseLight(new ConstantTexture(Eigen::Vector3d(30, 30, 30)));
  Material *glass = new Dielectric(2);
  //Hitable *light1 = new xz_rect(-100, 100, -100, 100, 299, light);
  Hitable *light1 = new xz_rect(skylight_min_x, skylight_max_x, skylight_min_z, skylight_max_z, skylight_max_y+1, light);
	Hitable *window1 = new RectangularPrism(Eigen::Vector3d(skylight_min_x-1, skylight_min_y, skylight_min_z-1), Eigen::Vector3d(skylight_max_x+1, skylight_max_y, skylight_max_z+1), glass);

  // Hitable *light2 = new xy_rect(windowLeft, windowRight, windowBottom, windowTop, windowBack-1, light);
	// Hitable *window2 = new RectangularPrism(Eigen::Vector3d(windowLeft-1, windowBottom-1, windowFront), Eigen::Vector3d(windowRight+1, windowTop+1, windowBack), glass);
	list.push_back(window1);
  //list.push_back(window2);
  lightList.push_back(light1);
  //lightList.push_back(light2);

  list.insert(list.end(), lightList.begin(), lightList.end());

  image.lights = new HitableList(lightList);

  //Material *glass = new Dielectric(2);
  Material *metal = new Metal(new ConstantTexture(Eigen::Vector3d(1, 1, 1)), 0.05);
  Material *red = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.65, 0.06, 0.06)), image.lights, mix);
  Material *white = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)), image.lights, mix);
  Material *green = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.12, 0.45, 0.15)), image.lights, mix);
  Material *blue = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.12, 0.12, 0.73)), image.lights, mix);
  Material *black = new Lambertian(new ConstantTexture(Eigen::Vector3d(0, 0, 0)), image.lights, mix);

  Material *checkerBoard = new Lambertian(new CheckerTexture(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)),
                                          new ConstantTexture(Eigen::Vector3d(0.12, 0.12, 0.12)), 0.05), image.lights, mix);
  Material *checkerBoard2 = new Lambertian(new CheckerTexture(new ConstantTexture(Eigen::Vector3d(80.0/255, 100.0/255, 0)),
                                                              new ConstantTexture(Eigen::Vector3d(1, 0, 1)), 0.05), image.lights, mix);
  Material *noise = new Lambertian(new NoiseTextureTwoColor(Eigen::Vector3d(0.73, 0.73, 0.73), Eigen::Vector3d(0.12, 0.12, 0.12), 0.005), image.lights, mix);


  //Left
  list.push_back(new yz_rect(-300, 300, -300, 300, 600, red));
  //right
  list.push_back(new yz_rect(-300, 300, -300, 300, -600, blue));
  //top
  //list.push_back(new xz_rect(-600, 600, -600, 300, 300, white));
  //bottom
  list.push_back(new xz_rect(-600, 600, -300, 300, -300, noise));
  // back
  list.push_back(new xy_rect(-600, 600, -300, 300, 300, green));

  //infront of camera
  list.push_back(new uni_xy_rect(-600, 600, -300, 300, -300, green,1));

  //left
  list.push_back(new RectangularPrism(Eigen::Vector3d(-600, skylight_min_y, -600), Eigen::Vector3d(skylight_min_x, skylight_max_y, 300), white));
  //right
  list.push_back(new RectangularPrism(Eigen::Vector3d(skylight_max_x, skylight_min_y, -600), Eigen::Vector3d(600, skylight_max_y, 300), white));
  //near
  list.push_back(new RectangularPrism(Eigen::Vector3d(skylight_min_x, skylight_min_y, -600), Eigen::Vector3d(skylight_max_x, skylight_max_y, skylight_min_z), white));
  //far
  list.push_back(new RectangularPrism(Eigen::Vector3d(skylight_min_x, skylight_min_y, skylight_max_z), Eigen::Vector3d(skylight_max_x, skylight_max_y, 300), white));

  // //top compound wall
  // list.push_back(new RectangularPrism(Eigen::Vector3d(-600, windowTop, windowFront), Eigen::Vector3d(600, 300, windowBack), blue));
  // //bottom compound wall
  // list.push_back(new RectangularPrism(Eigen::Vector3d(-600, -300, windowFront), Eigen::Vector3d(600, windowBottom, windowBack), blue));
  // //left  compound wall
  // list.push_back(new RectangularPrism(Eigen::Vector3d(-600, windowBottom, windowFront), Eigen::Vector3d(windowLeft, windowTop, windowBack), blue));
  // //right compound wall
  // list.push_back(new RectangularPrism(Eigen::Vector3d(windowRight, windowBottom, windowFront), Eigen::Vector3d(600, windowTop, windowBack), blue));

  std::vector<Sphere*> metaList;
  double randomDist = 100.0;
  double randomSize = 10;
  double baseSize = 5;
  double numBalls = 1000;
  for(int i = 0; i < numBalls; ++i) {
    Eigen::Vector3d center(randomDist*RandomHelper::randomNormal(),
                           randomDist*RandomHelper::randomNormal(),
                           randomDist*RandomHelper::randomNormal());
    double size = randomSize*RandomHelper::randomUniform()+baseSize;
    metaList.push_back(new Sphere(center, size, metal));
  }
  list.push_back(new MetaBall(metaList, glass));

  list.push_back(new ConstantMedium(new RectangularPrism(Eigen::Vector3d(-600, -300, -300), Eigen::Vector3d(600, 300, 300), glass), 0.0001, new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73))));


  std::vector<Moveable*> movers;
  std::vector<Constraint> constraints;

  image.world = new BVHNode(list);

  Bounds b;
  b.min = Eigen::Vector3d(-600, -300, -600);
  b.max = Eigen::Vector3d(600, 300, 1000);
  //                                                             gravity
  MoveableList moverList(movers, b, constraints, Eigen::Vector3d(0, -9.8, 0));
  moverList.setCollision(false); // change to true for collisions with other spheres

  image.backgroundColor = Eigen::Vector3d(0.0, 0.0, 0.0);
  Eigen::Vector3d lookfrom(0,0,-800-277.5);
  Eigen::Vector3d lookat(0,0,-277.5);
  image.cam.setUp(lookfrom, lookat, Eigen::Vector3d(0,1,0), 40, double(width)/double(height), 0.0, 1.0);
  return moverList;
}

void setUpScene(int width, int height, int numSamples, int timeout, int numThreads, double mix, double dt, int frames, std::string filename) {
  Image image1(width, height, numSamples, timeout, numThreads);
  MoveableList movers = cornell_box(image1, width, height, mix);

  bool didStop = false;
  int stopi = 0;

  for(int i = 0; i < frames; ++i) {
    image1.resetColor();
    image1.colorPixels();
    char buffer[5];
    std::snprintf(buffer, sizeof(buffer), "%04d", i);
    std::cout << filename + buffer + ".png" << std::endl;
    image1.writeToFile(filename + buffer + ".png");

    for(int j = 0; j < 1000; ++j) {
      movers.update(dt);
      image1.world->reset();
    }
  }
}

//ffmpeg -r 12 -i images/%04d.png -vcodec libx264 -crf 15 test.mp4

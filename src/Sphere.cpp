#include <limits>
#include <math.h>

#include "RandomHelper.h"

#include "Sphere.h"
#include "PDF.h"

bool Sphere::getTvals(const Ray& r, Eigen::Vector2d& tVals) const{
  Eigen::Vector3d oc = r.origin - center;
  double a = r.direction.dot(r.direction);
  double b = oc.dot(r.direction);
  double c = oc.dot(oc) - radius*radius;
  double discriminant = b*b - a*c;

  if(discriminant < 0) {
    return false;
  }

  double front = -b/a;
  double back = sqrt(discriminant)/a;
  tVals[0] = front - back;
  tVals[1] = front + back;
  return true;
}

bool Sphere::hit(const Ray& r, double t_min, double t_max, HitRecord& h) const{
  Eigen::Vector2d tVals;
  bool didHit = getTvals(r, tVals);
  if(didHit) {
    if (t_min < tVals(0) and tVals(0) < t_max) {
      h.t = tVals(0);
    } else if (t_min < tVals(1) and tVals(1) < t_max) {
      h.t = tVals(1);
    } else {
      return false;
    }

    h.point = r.pointAt(h.t);
    h.N = (h.point - center).normalized();
    h.matPtr = matPtr;
    return true;
  }
  return false;
}

bool Sphere::bounding_box(BoundingBox& box) const {
  Eigen::Vector3d rVec(radius, radius, radius);
  box = BoundingBox(center - rVec, center + rVec);
  return true;
}

Ray Sphere::random_ray_from_surface() const {
  Eigen::Vector3d origin = radius*RandomHelper::randomOnUnitSphere();
  Eigen::Vector3d N = (origin - center).normalized();
  Eigen::Vector3d target = (N + RandomHelper::randomInUnitSphere()).normalized();
  return Ray(origin, target);
}

double Sphere::distanceFrom(const Eigen::Vector3d& point) const {
  return (point-center).norm();
}

double Sphere::pdf_value(const Eigen::Vector3d& origin, const Eigen::Vector3d v) const {
  HitRecord h;
  if(this->hit(Ray(origin, v), 0.01, std::numeric_limits<double>::max(), h)) {
    double cos_theta_max = std::sqrt(1 - radius*radius/(center-origin).dot(center-origin));
    double solid_angle = 2*M_PI*(1-cos_theta_max);
    return 1/solid_angle;
  } else {
    return 0;
  }
}

inline Eigen::Vector3d random_to_sphere(double radius, double distance_squared) {
  double r1 = RandomHelper::randomUniform();
  double r2 = RandomHelper::randomUniform();
  double z = 1 + r2*(std::sqrt(1 - radius*radius/distance_squared) - 1);
  double phi = 2*M_PI*r1;
  double x = std::cos(phi)*std::sqrt(1-z*z);
  double y = std::sin(phi)*std::sqrt(1-z*z);
  return Eigen::Vector3d(x, y, z);
}

Eigen::Vector3d Sphere::random(const Eigen::Vector3d& origin) const {
  Eigen::Vector3d direction = center - origin;
  double distance_squared = direction.dot(direction);
  ONB uvw;
  uvw.build_from_w(direction);
  return uvw.local(random_to_sphere(radius, distance_squared)).normalized();
}

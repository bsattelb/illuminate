#include <eigen3/Eigen/Dense>
#include <iostream>
#include <omp.h>

#include "Scene.h"

int main(int argc, char* argv[]) {
  if(argc < 2) {
    std::cout << "Not enough file names entered.  Exiting." << std::endl;
    std::cout << "Usage: ./raytracer <filename.outputtype> --options" << std::endl <<
              "Options are selected from" << std::endl <<
              "  --resolution <width> <height>" << std::endl <<
              "  --samples <number of samples>" << std::endl <<
              "  --timeout <how many seconds to run at most>" << std::endl <<
              "  --threads <how many cores to run on>" << std::endl <<
              "  --mix <likelihood of indirect bounce>" << std::endl <<
              "    0 is only direct, 1 is fully indirect" << std::endl <<
              "  --dt <time step>" << std::endl <<
              "  --frames <number of frames>" << std::endl;
    return -1;
  }

  int width = 480;
  int height = 240;
  int numSamples = 100;
  int timeout = -1;
  int numThreads = 1;
  double mix = 0.5;
  double dt = 0.01;
  int frames = 1000;

  for (int i = 2; i < argc; ++i) {
    if (std::string(argv[i]) == "--resolution") {
      if (i + 1 < argc) {
        width = std::stoi(argv[++i]);
        height = std::stoi(argv[++i]);
      } else {
        std::cerr << "--resolution option requires two arguments." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--samples") {
      if (i + 1 < argc) {
        numSamples = std::stoi(argv[++i]);
      } else {
        std::cerr << "--samples option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--timeout") {
      if (i + 1 < argc) {
        timeout = std::stoi(argv[++i]);
      } else {
        std::cerr << "--timeout option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--threads") {
      if (i + 1 < argc) {
        numThreads = std::stoi(argv[++i]);
      } else {
        std::cerr << "--threads option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--mix") {
      if (i + 1 < argc) {
        mix = std::stod(argv[++i]);
      } else {
        std::cerr << "--mix option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--dt") {
      if (i + 1 < argc) {
        dt = std::stod(argv[++i]);
      } else {
        std::cerr << "--dt option requires one argument." << std::endl;
        return 1;
      }
    } else if (std::string(argv[i]) == "--frames") {
      if (i + 1 < argc) {
        frames = std::stoi(argv[++i]);
      } else {
        std::cerr << "--frames option requires one argument." << std::endl;
        return 1;
      }
    }
  }

  // Image image(width, height, numSamples, timeout, numThreads);
  // image.colorPixels();
  // image.writeToFile(argv[1]);
  setUpScene(width, height, numSamples, timeout, numThreads, mix, dt, frames, argv[1]);

  return 0;
}

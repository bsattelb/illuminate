#include "HitableList.h"

#include <iostream>
#include <vector>
#include <tuple>
#include "Hitable.h"
#include "BoundingBox.h"
#include "RandomHelper.h"

bool HitableList::hit(const Ray& r, double t_min, double t_max, HitRecord& h) const {
  HitRecord tempRec;
  bool hitAnything = false;
  double bestT = t_max;
  for(int i = 0; i < list.size(); i++) {
    if(list[i]->hit(r, t_min, bestT, tempRec) /*&& tempRec.N.dot(r.direction) < 0*/) {
      hitAnything = true;
      bestT = tempRec.t;
      h = tempRec;
    }
  }
  return hitAnything;
}

bool HitableList::bounding_box(BoundingBox& box) const {
  if(list.size() < 1) {
    return false;
  }
  BoundingBox tempBox;
  bool firstTrue = list[0]->bounding_box(tempBox);
  if(!firstTrue) {
    return false;
  }
  else {
    box = tempBox;
  }
  for(int i = 1; i < list.size(); i++) {
    if(list[i]->bounding_box(tempBox)) {
      box = BoundingBox::surrounding_box(box, tempBox);
    }
    else {
      return false;
    }
  }
  return true;
}

Ray HitableList::random_ray_from_surface() const {
  int index = RandomHelper::randomInt(0, list.size()-1);
  return list[index]->random_ray_from_surface();
}

double HitableList::pdf_value(const Eigen::Vector3d& origin, const Eigen::Vector3d v) const {
  double sum = 0;
  for(int i = 0; i < list.size(); ++i) {
    sum += list[i]->pdf_value(origin, v)/list.size();
  }
  return sum;
}
Eigen::Vector3d HitableList::random(const Eigen::Vector3d& origin) const {
  return list[RandomHelper::randomInt(0, list.size()-1)]->random(origin);
}

#pragma once

#include <eigen3/Eigen/Dense>
#include <math.h>
#include "Ray.h"
#include "Hitable.h"
#include "RandomHelper.h"

inline Eigen::Vector3d random_cosine_direction() {
  double r1 = RandomHelper::randomUniform();
  double r2 = RandomHelper::randomUniform();
  double z = std::sqrt(1-r2);
  double phi = 2*M_PI*r1;
  double x = std::cos(phi)*2*std::sqrt(r2);
  double y = std::sin(phi)*2*std::sqrt(r2);
  return Eigen::Vector3d(x, y, z);
}

class ONB {
public:
  Eigen::Vector3d u, v, w;
  ONB() {}
  Eigen::Vector3d local(double a, double b, double c) const { return a*u + b*v + c*w;}
  Eigen::Vector3d local(const Eigen::Vector3d& a) const { return a(0)*u + a(1)*v + a(2)*w;}
  void build_from_w(const Eigen::Vector3d& n) {
    w = n.normalized();
    Eigen::Vector3d a;
    if(std::abs(w(0)) > 0.9) {
      a = Eigen::Vector3d(0, 1, 0);
    } else {
      a = Eigen::Vector3d(1, 0, 0);
    }
    v = w.cross(a).normalized();
    u = w.cross(v);
  }
};

class PDF {
public:
  virtual double value(const Eigen::Vector3d& direction) const = 0;
  virtual Eigen::Vector3d generate() const = 0;
};

class CosinePDF : public PDF {
public:
  ONB uvw;

  CosinePDF(const Eigen::Vector3d w) {uvw.build_from_w(w);}
  virtual double value(const Eigen::Vector3d& direction) const override {
    double cosine = uvw.w.dot(direction.normalized());
    if(cosine > 0) {
      return cosine/M_PI;
    } else {
      return 0;
    }
  }

  virtual Eigen::Vector3d generate() const override {
    return uvw.local(random_cosine_direction());
  }
};

class HitablePDF : public PDF {
public:
  Eigen::Vector3d origin;
  Hitable *ptr;
  HitablePDF(Hitable * p, const Eigen::Vector3d& origin) : ptr(p), origin(origin) {}
  virtual double value(const Eigen::Vector3d& direction) const override {
    return ptr->pdf_value(origin, direction);
  }
  virtual Eigen::Vector3d generate() const override {
    return ptr->random(origin);
  }
};

class MixturePDF : public PDF {
public:
  PDF* p1;
  PDF* p2;
  double mix;
  MixturePDF(PDF *p1, PDF* p2, double mix) : p1(p1), p2(p2), mix(mix) {}
  virtual double value(const Eigen::Vector3d& direction) const override {
    return mix*p1->value(direction) + (1-mix)*p2->value(direction);
  }
  virtual Eigen::Vector3d generate() const override {
    if(RandomHelper::randomUniform() < mix) {
      return p1->generate();
    } else {
      return p2->generate();
    }
  }
};

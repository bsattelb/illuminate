#pragma once

#include <eigen3/Eigen/Dense>
#include <vector>

#include "Hitable.h"

struct Constraint {
  double equilibrium;
  double k;
  Moveable * obj1;
  Moveable * obj2;
  Constraint(double k, double e, Moveable* o1, Moveable* o2) : equilibrium(e), obj1(o1), obj2(o2), k(k) {}

  void updateAccelerations() {
    double dist = (obj1->getLoc() - obj2->getLoc()).norm();
    Eigen::Vector3d force = k*(dist - equilibrium)*(obj2->getLoc() - obj1->getLoc())/dist;
    obj1->acceleration += force/obj1->mass;
    obj2->acceleration += -force/obj2->mass;
  }
};

struct Collider {
  std::vector<Moveable *> objs;

  Collider(std::vector<Moveable*> o) : objs(o) {}

  void checkCollisions() {
    for(int i = 0; i < objs.size(); ++i) {
      double radius1 = 0;
      Eigen::Vector3d center1(0,0,0);
      if(objs[i]->object->doesCollide(center1, radius1)) {
        for(int j =i+1; j < objs.size(); ++j) {
          double radius2 = 0;
          Eigen::Vector3d center2(0,0,0);
          if(objs[j]->object->doesCollide(center2, radius2)) {
            double dist = (center1 - center2).norm();
            if(dist < radius1 + radius2) {
              Eigen::Vector3d n = (center1 - center2).normalized();
              double proj = (objs[i]->velocity - objs[j]->velocity).dot(n);

              double temp = 2/(objs[i]->mass + objs[j]->mass)*proj;

              objs[i]->velocity = objs[i]->velocity - objs[i]->mass*temp*n;
              objs[j]->velocity = objs[j]->velocity + objs[j]->mass*temp*n;
            }
          }
        }
      }
    }
  }
};


class MoveableList {
public:
  std::vector<Moveable *> movers;
  Bounds bound;
  std::vector<Constraint> constraints;
  Eigen::Vector3d gravity;
  bool doCollisions = false;
  Collider * collider;

  MoveableList() {}
  MoveableList(std::vector<Moveable*> m) : movers(m) {gravity=Eigen::Vector3d(0, 0, 0);}
  MoveableList(std::vector<Moveable*> m, Bounds b) : movers(m), bound(b) {gravity=Eigen::Vector3d(0, 0, 0);}
  MoveableList(std::vector<Moveable*> m, Bounds b, std::vector<Constraint> c) : movers(m), bound(b), constraints(c) {gravity=Eigen::Vector3d(0, 0, 0);}
  MoveableList(std::vector<Moveable*> m, Bounds b, std::vector<Constraint> c, Eigen::Vector3d g) : movers(m), bound(b), constraints(c), gravity(g) {}

  void setCollision(bool doCollide) {
    doCollisions = doCollide;
    if(doCollisions == true) {
      collider = new Collider(movers);
    }
  }

  void update(double dt) {
    for(int i = 0; i < movers.size(); ++i) {
      movers[i]->updatePosition(dt, bound);
      movers[i]->setOldAcceleration();
    }
    for(int i = 0; i < constraints.size(); ++i) {
      constraints[i].updateAccelerations();
    }
    for(int i = 0; i < movers.size(); ++i) {
      movers[i]->acceleration += dt*gravity;
    }
    if(doCollisions) {
      collider->checkCollisions();
    }

    for(int i = 0; i < movers.size(); ++i) {
      movers[i]->updateVelocity(dt);
    }

  }
};

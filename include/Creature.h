#pragma once

#include <vector>
#include <eigen3/Eigen/Dense>
#include <math.h>

#include "Sphere.h"
#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"
#include "Material.h"
#include "Texture.h"
#include "Rectangle.h"
#include "MetaBall.h"
#include "Moveable.h"
#include "MoveableList.h"

// Make a line of numBalls spheres from loc1 to loc2, each with radius sphereRadius
std::vector<Sphere*> makeLine(Eigen::Vector3d loc1, Eigen::Vector3d loc2, double sphereRadius, int numBalls = 10) {
  Eigen::Vector3d length = loc2 - loc1;
  Eigen::Vector3d step = length/numBalls;
  Material * mat = NULL;
  std::vector<Sphere*> sphereList;
  for(int i = 0; i <= numBalls; ++i) {
    Eigen::Vector3d pos = loc1 + i*step;
    sphereList.push_back(new Sphere(pos, sphereRadius, mat));
  }
  return sphereList;
}

// For every sphere in sphereList, add it to fullList and a new list of Moveable objects that is returned
std::vector<Moveable*> turnIntoMovers(std::vector<Sphere*> &sphereList, std::vector<Moveable*> &fullList) {
  std::vector<Moveable*> movers;
  for(int i = 0; i < sphereList.size(); ++i) {
    Moveable * moveable = new Moveable(sphereList[i], Eigen::Vector3d(0, 0, 0));
    movers.push_back(moveable);
    fullList.push_back(moveable);
  }
  return movers;
}

// For every two movering objects in movers, create a constraint between them with springiness k and add it to constraints
void makeLimbConstraints(double k, std::vector<Moveable*> &movers, std::vector<Constraint> &constraints) {
  for(int i = 0; i < movers.size(); ++i) {
    for(int j = i+1; j < movers.size(); ++j) {
      double equilibrium = (movers[i]->getLoc() - movers[j]->getLoc()).norm();
        constraints.push_back(Constraint(k, equilibrium, movers[i], movers[j]));
    }
  }
}

std::vector<Hitable*> makeCreature(std::vector<Moveable*> &movers, std::vector<Constraint> &constraints, Material * mat) {
  std::vector<Hitable*> list;

  int numBalls = 20;
  double radius = 20;
  double limbStrength = 10;
  double jointStrength = 100;
  double cheatStrength = 1;

  std::vector<Sphere*> allParts;

  // Make head (you could put facial features on this :)
  std::vector<Sphere*> head;  // Make a list containing all the head pieces
  head.push_back(new Sphere(Eigen::Vector3d(0, 101, 0), radius*2, mat)); // Add the head sphere
  std::vector<Moveable*> headMovers = turnIntoMovers(head, movers); // Turn that into a moveable object
  allParts.insert(allParts.end(), head.begin(), head.end()); // Add the head to the list of all spheres

  // Make spine
  // Make a line going between two points out of spheres
  std::vector<Sphere*> spine = makeLine(Eigen::Vector3d(0,-100,0), Eigen::Vector3d(0, 100, 0), radius, numBalls);
  // Turn those spheres into moveable objects
  std::vector<Moveable*> spineMovers = turnIntoMovers(spine, movers);
  // Add constraints between every sphere in the spine
  makeLimbConstraints(limbStrength, spineMovers, constraints);
  // Add all the spine pieces to the list of all spheres
  allParts.insert(allParts.end(), spine.begin(), spine.end());

  // Make arm
  std::vector<Sphere*> arm1 = makeLine(Eigen::Vector3d(1,0,0), Eigen::Vector3d(100, 100, 0), radius, numBalls);
  std::vector<Moveable*> arm1Movers = turnIntoMovers(arm1, movers);
  makeLimbConstraints(limbStrength, arm1Movers, constraints);
  allParts.insert(allParts.end(), arm1.begin(), arm1.end());

  // Make arm
  std::vector<Sphere*> arm2 = makeLine(Eigen::Vector3d(-1,0,0), Eigen::Vector3d(-100, 100, 0), radius, numBalls);
  std::vector<Moveable*> arm2Movers = turnIntoMovers(arm2, movers);
  makeLimbConstraints(limbStrength, arm2Movers, constraints);
  allParts.insert(allParts.end(), arm2.begin(), arm2.end());

  // Make leg
  std::vector<Sphere*> leg1 = makeLine(Eigen::Vector3d(-1,-100,0), Eigen::Vector3d(-100, -200, 0), radius, numBalls);
  std::vector<Moveable*> leg1Movers = turnIntoMovers(leg1, movers);
  makeLimbConstraints(limbStrength, leg1Movers, constraints);
  allParts.insert(allParts.end(), leg1.begin(), leg1.end());

  // Make leg
  std::vector<Sphere*> leg2 = makeLine(Eigen::Vector3d(1,-100,0), Eigen::Vector3d(100, -200, 0), radius, numBalls);
  std::vector<Moveable*> leg2Movers = turnIntoMovers(leg2, movers);
  makeLimbConstraints(limbStrength, leg2Movers, constraints);
  allParts.insert(allParts.end(), leg2.begin(), leg2.end());

  // Add all objects to a metaball
  list.push_back(new MetaBall(allParts, mat));

  // Add head to spine
  double equilibrium = (headMovers[0]->getLoc() - spineMovers[numBalls-1]->getLoc()).norm();
  constraints.push_back(Constraint(jointStrength, equilibrium, headMovers[0], spineMovers[numBalls-1]));

  // Add arm to spine
  equilibrium = (spineMovers[numBalls/2]->getLoc() - arm1Movers[0]->getLoc()).norm();
  constraints.push_back(Constraint(jointStrength, equilibrium, spineMovers[numBalls/2], arm1Movers[0]));

  // Add leg to spine
  equilibrium = (spineMovers[numBalls-1]->getLoc() - leg1Movers[0]->getLoc()).norm();
  constraints.push_back(Constraint(jointStrength, equilibrium, spineMovers[numBalls-1], leg1Movers[0]));

  // Add other arm to spine
  equilibrium = (spineMovers[numBalls/2]->getLoc() - arm2Movers[0]->getLoc()).norm();
  constraints.push_back(Constraint(jointStrength, equilibrium, spineMovers[numBalls/2], arm2Movers[0]));

  // Add other leg to spine
  equilibrium = (spineMovers[numBalls-1]->getLoc() - leg2Movers[0]->getLoc()).norm();
  constraints.push_back(Constraint(jointStrength, equilibrium, spineMovers[numBalls-1], leg2Movers[0]));

  // Connect hands to preserve locations
  equilibrium = (arm1Movers[numBalls-1]->getLoc() - arm2Movers[numBalls-1]->getLoc()).norm();
  constraints.push_back(Constraint(cheatStrength, equilibrium, arm1Movers[numBalls-1], arm2Movers[numBalls-1]));

  // Connect legs to preserve locations
  equilibrium = (leg1Movers[numBalls-1]->getLoc() - leg2Movers[numBalls-1]->getLoc()).norm();
  constraints.push_back(Constraint(cheatStrength, equilibrium, leg1Movers[numBalls-1], leg2Movers[numBalls-1]));

  // Connect hands and legs to preserve structure
  equilibrium = (arm1Movers[numBalls-1]->getLoc() - leg1Movers[numBalls-1]->getLoc()).norm();
  constraints.push_back(Constraint(cheatStrength, equilibrium, arm1Movers[numBalls-1], leg1Movers[numBalls-1]));
  equilibrium = (arm2Movers[numBalls-1]->getLoc() - leg2Movers[numBalls-1]->getLoc()).norm();
  constraints.push_back(Constraint(cheatStrength, equilibrium, arm2Movers[numBalls-1], leg2Movers[numBalls-1]));

  return list;
}

/*
MoveableList testCreatureScene(Image &image, int width, int height) {
  std::vector<Hitable*> list;

  std::vector<Hitable*> lightList;
  Material *light = new DiffuseLight(new ConstantTexture(Eigen::Vector3d(15, 15, 15)));
  Hitable *light1 = new xz_rect(-100, 100, -100, 100, 299, light);
  lightList.push_back(light1);
  list.insert(list.end(), lightList.begin(), lightList.end());
  image.lights = new HitableList(lightList);

  double mix = 0.5;
  Material *white = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.73, 0.73)), image.lights, mix);
  Material *red = new Lambertian(new ConstantTexture(Eigen::Vector3d(0.73, 0.1, 0.1)), image.lights, mix);

  list.push_back(new yz_rect(-300, 300, -500, 300, 600, red));
  list.push_back(new yz_rect(-300, 300, -500, 300, -600, red));
  list.push_back(new xz_rect(-600, 600, -500, 300, 300, red));
  list.push_back(new xz_rect(-600, 600, -500, 300, -300, red));

  list.push_back(new xy_rect(-600, 600, -300, 300, 300, red));
  list.push_back(new uni_xy_rect(-600, 600, -300, 300, -500, red, 1));

  Eigen::Vector3d lookfrom(0,0,-1200);
  Eigen::Vector3d lookat(0,0,0);
  image.cam.setUp(lookfrom, lookat, Eigen::Vector3d(0,1,0), 40, double(width)/double(height), 0.0, 1.0);

  std::vector<Moveable*> movers;
  std::vector<Constraint> constraints;

  std::vector<Hitable*> creature = makeCreature(movers, constraints, white);
  list.insert(list.end(), creature.begin(), creature.end());

  image.world = new HitableList(list);
  Bounds b;
  b.min = Eigen::Vector3d(-600, -300, -300);
  b.max = Eigen::Vector3d(600, 300, 300);
  return MoveableList(movers, b, constraints, Eigen::Vector3d(0, -1, 0));
}
*/

#pragma once

#include <eigen3/Eigen/Dense>
#include <vector>

#include "Hitable.h"

struct Bounds {
  Eigen::Vector3d min, max;
};

class Moveable {
public:
  Hitable *object;
  Eigen::Vector3d velocity;
  Eigen::Vector3d oldAcceleration;
  Eigen::Vector3d acceleration;
  double mass;

  Moveable(Hitable *o) : object(o) {mass=1;velocity=Eigen::Vector3d(0,0,0);acceleration=Eigen::Vector3d(0,0,0);}
  Moveable(Hitable *o, Eigen::Vector3d v) : object(o), velocity(v) {mass = 1;acceleration=Eigen::Vector3d(0,0,0);}

  void updatePosition(double dt, Bounds bound) {
    object->updatePosition(dt*velocity + 0.5*dt*dt*acceleration);
    BoundingBox b1;
    if(object->bounding_box(b1)) {
      for(int i = 0; i < 3; ++i) {
        if(b1.min(i) < bound.min(i)) {
          velocity(i) = -velocity(i);
        }
        if(b1.max(i) > bound.max(i)) {
          velocity(i) = -velocity(i);
        }
      }
    }
  }

  void setOldAcceleration() {
    oldAcceleration = acceleration;
    acceleration = Eigen::Vector3d(0, 0, 0);
  }

  void updateVelocity(double dt) {
    velocity += 0.5*dt*(acceleration + oldAcceleration);
  }

  Eigen::Vector3d getLoc() {
    BoundingBox b1;
    if(object->bounding_box(b1)) {
      return (b1.max + b1.min)/2;
    }
    return Eigen::Vector3d(0, 0, 0);
  }
};

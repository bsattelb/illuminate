#pragma once

#include "Image.h"
#include <string>

void setUpScene(int width, int height, int numSamples, int timeout, int numThreads, double mix, double dt, int frames, std::string filename);

#pragma once
#include <eigen3/Eigen/Dense>
#include <tuple>
#include <limits>

#include "Hitable.h"
#include "HitableList.h"
#include "RandomHelper.h"

class Rect : public Hitable {
public:
  double x0, x1, y0, y1, k;
  Eigen::Vector3d N;
  int axis;

  Rect() {};
  Rect(double _x0, double _x1, double _y0, double _y1, double _k, int axis, Material *mat) : x0(_x0),
    x1(_x1), y0(_y0), y1(_y1), k(_k), axis(axis) {
    matPtr = mat;
    N = Eigen::Vector3d(0, 0, 0);
    N(axis) = 1;
  }

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override {
    double t = (k - r.origin(axis)) / r.direction(axis);
    if(t < t_min || t > t_max) {
      return false;
    }

    double x;
    double y;
    switch(axis) {
      case 0:
        x = r.origin(1) + t*r.direction(1);
        y = r.origin(2) + t*r.direction(2);
        break;
      case 1:
        x = r.origin(0) + t*r.direction(0);
        y = r.origin(2) + t*r.direction(2);
        break;
      case 2:
        x = r.origin(0) + t*r.direction(0);
        y = r.origin(1) + t*r.direction(1);
        break;
    }

    if(x < x0 || x > x1 || y < y0 || y > y1) {
      return false;
    }

    h.u = (x-x0)/(x1-x0);
    h.v = (y-y0)/(y1-y0);
    h.t = t;
    h.point = r.pointAt(t);

    if(N.dot(r.direction) < 0) {
      h.N = N;
    } else {
      h.N = -N;
    }

    h.matPtr = matPtr;
    return true;
  }

  virtual bool bounding_box(BoundingBox& box) const override {
    switch(axis) {
      case 0:
        box = BoundingBox(Eigen::Vector3d(k-0.0001, x0, y0), Eigen::Vector3d(k+0.00001, x1, y1));
        break;
      case 1:
        box = BoundingBox(Eigen::Vector3d(x0, k-0.0001, y0), Eigen::Vector3d(x1, k+0.0001, y1));
        break;
      case 2:
        box = BoundingBox(Eigen::Vector3d(x0, y0, k-0.0001), Eigen::Vector3d(x1, y1, k+0.0001));
        break;
    }
    return true;
  }

  virtual Ray random_ray_from_surface() const override {
    Eigen::Vector3d origin;
    origin(axis) = k;
    switch(axis) {
      case 0:
        origin(1) = (x1-x0)*RandomHelper::randomUniform() + x0;
        origin(2) = (y1-y0)*RandomHelper::randomUniform() + y0;
        break;
      case 1:
        origin(0) = (x1-x0)*RandomHelper::randomUniform() + x0;
        origin(2) = (y1-y0)*RandomHelper::randomUniform() + y0;
        break;
      case 2:
        origin(0) = (x1-x0)*RandomHelper::randomUniform() + x0;
        origin(1) = (y1-y0)*RandomHelper::randomUniform() + y0;
        break;
    }
    Eigen::Vector3d target = (N + RandomHelper::randomInUnitSphere()).normalized();
    return Ray(origin, target);
  }

  virtual double pdf_value(const Eigen::Vector3d& origin, const Eigen::Vector3d v) const override {
    HitRecord h;
    if(this->hit(Ray(origin, v), 0.001, std::numeric_limits<double>::max(), h)) {
      double area = (x1-x0)*(y1-y0);
      double distSquared = h.t*h.t;
      double cosine = std::abs(v.dot(h.N));
      return distSquared/(cosine*area);
    } else {
      return 0;
    }
  }
  virtual Eigen::Vector3d random(const Eigen::Vector3d& origin) const override {
    Eigen::Vector3d randomPoint;
    switch(axis) {
      case 0:
        randomPoint = Eigen::Vector3d(k, x0 + (x1-x0)*RandomHelper::randomUniform(), y0 + (y1-y0)*RandomHelper::randomUniform());
        break;
      case 1:
        randomPoint = Eigen::Vector3d(x0 + (x1-x0)*RandomHelper::randomUniform(), k, y0 + (y1-y0)*RandomHelper::randomUniform());
        break;
      case 2:
        randomPoint = Eigen::Vector3d(x0 + (x1-x0)*RandomHelper::randomUniform(), y0 + (y1-y0)*RandomHelper::randomUniform(), k);
        break;
    }
    return (randomPoint - origin).normalized();
  }
};

class xy_rect : public Rect {
public:
  xy_rect() {}
  xy_rect(double _x0, double _x1, double _y0, double _y1, double _k, Material *mat) : Rect(_x0, _x1, _y0, _y1, _k, 2, mat) {}
};

class xz_rect : public Rect {
public:
  xz_rect() {}
  xz_rect(double _x0, double _x1, double _y0, double _y1, double _k, Material *mat) : Rect(_x0, _x1, _y0, _y1, _k, 1, mat) {}
};

class yz_rect : public Rect {
public:
  yz_rect() {}
  yz_rect(double _x0, double _x1, double _y0, double _y1, double _k, Material *mat) : Rect(_x0, _x1, _y0, _y1, _k, 0, mat) {}
};

class uni_xy_rect : public Rect {
public:
  uni_xy_rect() {};
  uni_xy_rect(double _x0, double _x1, double _y0, double _y1, double _k, Material *mat, int direction) : Rect(_x0, _x1, _y0, _y1, _k, 2, mat) {N = direction*N;};
  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override {
    if(r.direction.dot(N) > 0) {
      return false;
    }
    return Rect::hit(r, t_min, t_max, h);
  }
};

class RectangularPrism : public HitableList {
  Eigen::Vector3d low;
  Eigen::Vector3d high;
  Material * matPtr;
public:
  RectangularPrism() {};
  RectangularPrism(Eigen::Vector3d low, Eigen::Vector3d high, Material * mat) : low(low), high(high){
    Material * dummyMaterial;
    list.push_back(new yz_rect(low(1), high(1), low(2), high(2), low(0), dummyMaterial));
    list.push_back(new yz_rect(low(1), high(1), low(2), high(2), high(0), dummyMaterial));
    list.push_back(new xz_rect(low(0), high(0), low(2), high(2), low(1), dummyMaterial));
    list.push_back(new xz_rect(low(0), high(0), low(2), high(2), high(1), dummyMaterial));
    list.push_back(new xy_rect(low(0), high(0), low(1), high(1), low(2), dummyMaterial));
    list.push_back(new xy_rect(low(0), high(0), low(1), high(1), high(2), dummyMaterial));
    matPtr = mat;
  }

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const override {
    if(HitableList::hit(r, t_min, t_max, h)) {
        h.matPtr = matPtr;
        return true;
    }
    return false;
  }

  virtual bool inside(const Eigen::Vector3d point) const override {
    for(int i = 0; i < 3; ++i) {
      if(low(0) > point(0) || point(0) > high(0)) {
        return false;
      }
    }

    return true;
  }
};

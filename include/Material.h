#pragma once

#include <iostream>

#include <eigen3/Eigen/Dense>
#include <math.h>
#include "Ray.h"
#include "Hitable.h"
#include "RandomHelper.h"
#include "Texture.h"
#include "PDF.h"

inline Eigen::Vector3d reflect(const Eigen::Vector3d& v, const Eigen::Vector3d N) {
  return v - 2*v.dot(N)*N;
}

inline bool refract(const Eigen::Vector3d& v, const Eigen::Vector3d& N, double niOvernt, Eigen::Vector3d& refracted) {
  double dt = v.dot(N);
  double discriminant = 1.0 - niOvernt*niOvernt*(1 - dt*dt);
  if (discriminant > 0) {
    refracted = niOvernt*(v - N*dt) - N*sqrt(discriminant);
    return true;
  }
  return false;
}

inline double schlick(double cosine, double refractiveIndex) {
  double r0 = (1 - refractiveIndex) / (1 + refractiveIndex);
  r0 = r0*r0;
  return r0 + (1 - r0)*pow(1 - cosine, 5);
}

class Material {
public:
  virtual bool scatter(const Ray& r, const HitRecord& h, Eigen::Vector3d& attenuation, Ray& scattered) const = 0;
  virtual Eigen::Vector3d emitted(double u, double v, const HitRecord& h, const Ray& r) const {
    return Eigen::Vector3d(0,0,0);
  }
  virtual double scattering_pdf(const Ray& r, const HitRecord& h, const Ray& scattered) const {
    return 1.0;
  }
};

class Lambertian: public Material {
public:
  Texture* albedo;
  Hitable *light;
  double mix;
  Lambertian(Texture* a) : albedo(a), mix(1) {}
  Lambertian(Texture* a, Hitable *light) : albedo(a), light(light), mix(0.5) {}
  Lambertian(Texture* a, Hitable *light, double mix) : albedo(a), light(light), mix(mix) {}
  virtual bool scatter(const Ray& r, const HitRecord& h, Eigen::Vector3d& attenuation, Ray& scattered) const override{
    if(mix < 1) {
      CosinePDF cospdf(h.N);
      HitablePDF hitpdf(light, h.point);
      MixturePDF mixpdf(&cospdf, &hitpdf, mix);
      scattered = Ray(h.point, mixpdf.generate());
      double pdf = mixpdf.value(scattered.direction);
      double probVal = h.matPtr->scattering_pdf(r, h, scattered)/pdf;
      attenuation = probVal*albedo->value(0, 0, h.point);
      return true;
    } else {
      CosinePDF cospdf(h.N);
      scattered = Ray(h.point, cospdf.generate());
      double pdf = cospdf.value(scattered.direction);
      double probVal = h.matPtr->scattering_pdf(r, h, scattered)/pdf;
      attenuation = probVal*albedo->value(0, 0, h.point);
      return true;
    }
  }
  virtual double scattering_pdf(const Ray& r, const HitRecord& h, const Ray& scattered) const override {
    double cosine = h.N.dot(scattered.direction);
    if(cosine <0) {
      cosine = 0;
    }
    return cosine/M_PI;
  }
};

class Metal : public Material {
public:
  Texture* albedo;
  double fuzz;
  Metal(Texture* attenuation, double f) {
    albedo = attenuation;
    if(f < 1) {
      fuzz = f;
    } else {
      fuzz = 1;
    }
  }
  virtual bool scatter(const Ray& r, const HitRecord& h, Eigen::Vector3d& attenuation, Ray& scattered) const override{
      Eigen::Vector3d target = reflect(r.direction, h.N);
      target = (target + fuzz*RandomHelper::randomInUnitSphere()).normalized();
      scattered = Ray(h.point, target);
      attenuation = albedo->value(0, 0, h.point);
      return scattered.direction.dot(h.N) > 0;
  }
};

class Dielectric : public Material {
public:
  double refractiveIndex;
  Eigen::Vector3d albedo;
  Dielectric(float ri) {
    refractiveIndex = ri;
    albedo(0) = 1;
    albedo(1) = 1;
    albedo(2) = 1;
  }

  Dielectric(float ri, const Eigen::Vector3d& attenuation) {
    refractiveIndex = ri;
    albedo = attenuation;
  }

  virtual bool scatter(const Ray& r, const HitRecord& h, Eigen::Vector3d& attenuation, Ray& scattered) const override{
    Eigen::Vector3d reflected = reflect(r.direction, h.N);
    double niOvernt;
    attenuation = Eigen::Vector3d(1,1,1);
    if(!r.insideRefractive) {
      for(int i = 0; i < 3; ++i) {
        attenuation(i) = std::exp(-h.t/50*(1 - albedo(i)));
      }
    }

    Eigen::Vector3d refracted;
    Eigen::Vector3d tempN;

    double reflectProb;
    double cosine;

    if(r.direction.dot(h.N) > 0) {
      tempN = -h.N;
      niOvernt = refractiveIndex;
      cosine = r.direction.dot(h.N);
      cosine = sqrt(1 - refractiveIndex*refractiveIndex*(1 - cosine*cosine));
    } else {
      tempN = h.N;
      niOvernt = 1.0/refractiveIndex;
      cosine = -r.direction.dot(h.N);
    }

    if (refract(r.direction, tempN, niOvernt, refracted)) {
      reflectProb = schlick(cosine, refractiveIndex);
    } else {
      reflectProb = 1.0;
    }

    if(RandomHelper::randomUniform() < reflectProb) {
      scattered = Ray(h.point, reflected);
    } else {
      scattered = Ray(h.point, refracted);
      r.insideRefractive = !r.insideRefractive;
    }



    return true;
  }
};

class DiffuseLight : public Material {
public:
  Texture *emit;
  DiffuseLight(Texture *a) : emit(a) {}
  virtual bool scatter(const Ray& r, const HitRecord& h, Eigen::Vector3d& attenuation, Ray& scattered) const override {
    return false;
  }
  virtual Eigen::Vector3d emitted(double u, double v, const HitRecord& h, const Ray& r) const override {
      if(h.N.dot(r.direction) < 0) {
        return emit->value(u,v,h.point);
      } else {
        return Eigen::Vector3d(0,0,0);
      }
  }
};

class Isotropic : public Material {
public:
  Texture* albedo;

  Isotropic(Texture * t) : albedo(t) {}

  virtual bool scatter(const Ray& r, const HitRecord& h, Eigen::Vector3d& attenuation, Ray& scattered) const override{
      scattered = Ray(h.point, RandomHelper::randomInUnitSphere());
      attenuation = albedo->value(0, 0, h.point);
      return true;
  }
};

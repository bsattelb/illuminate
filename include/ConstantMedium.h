#pragma once

#include <math.h>
#include <eigen3/Eigen/Dense>
#include <limits>
#include "Material.h"
#include "Texture.h"
#include "Hitable.h"
#include "Ray.h"

#include <iostream>


class ConstantMedium : public Hitable {
  Hitable *boundary;
  Material *phase_function;
  double neg_inv_density;
  bool insideGlass;
public:
  ConstantMedium() {}
  ConstantMedium(Hitable *b, double d, Texture* a, bool inside=false) : boundary(b), neg_inv_density(-1/d), insideGlass(inside){
    phase_function = new Isotropic(a);
  }

  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const {
    HitRecord rec1;
    HitRecord rec2;

    if(insideGlass ^ r.insideRefractive) {
      return false;
    }

    if(!boundary->hit(r, -std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), rec1)) {
      return false;
    }
    if(!boundary->hit(r, rec1.t+0.0001, std::numeric_limits<double>::max(), rec2)) {
      return false;
    }

    if(rec1.t < t_min) {
      rec1.t = t_min;
    }
    if(rec1.t < 0) {
      rec1.t = 0;
    }
    if(rec2.t > t_max) {
      rec2.t = t_max;
    }
    //std::cout << rec1.t << " " << rec2.t << std::endl;
    if(rec1.t >= rec2.t) {
      return false;
    }
    //std::cout << "hey3" << std::endl;

    double distanceInsideBoundary = rec2.t - rec1.t;
    double hitDist = neg_inv_density * std::log(RandomHelper::randomUniform());

    if(hitDist > distanceInsideBoundary) {
      return false;
    }
    h.t = rec1.t + hitDist;
    h.point = r.pointAt(h.t);
    h.N = Eigen::Vector3d(0, 0, 0);
    h.matPtr = phase_function;

    return true;
  }

  virtual bool bounding_box(BoundingBox& box) const {
    return boundary->bounding_box(box);
  }
  virtual Ray random_ray_from_surface() const {
    return boundary->random_ray_from_surface();
  }
};

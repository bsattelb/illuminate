#pragma once

#include <string>
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <utility>
#include <vector>
#include "Ray.h"
#include "Hitable.h"
#include "Camera.h"


class Image {
private:
  Eigen::Vector3d raytrace(Ray r);
  Eigen::Vector3d color(Ray r, int depth, Eigen::Vector3d& albedo);


  int width, height, numSamples, maxDepth, timeout, numThreads;
  cv::Mat image;

public:
  Hitable *world;
  Hitable *lights;
  Camera cam;
  Eigen::Vector3d backgroundColor;
  Image(int width, int height, int numSamples, int timeout, int numThreads);
  void colorPixels();
  void writeToFile(std::string filename);
  void resetColor();
};

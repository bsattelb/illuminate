#pragma once

#include "Ray.h"
#include "BoundingBox.h"
#include <tuple>

class Material;

struct HitRecord {
  double t;
  double u, v;
  Eigen::Vector3d N;
  Eigen::Vector3d point;
  Material *matPtr;
};

class Hitable {
public:
  Material *matPtr;
  virtual bool hit(const Ray& r, double t_min, double t_max, HitRecord& h) const = 0;
  virtual bool bounding_box(BoundingBox& box) const = 0;
  virtual Ray random_ray_from_surface() const = 0;

  virtual bool inside(const Eigen::Vector3d point) const {
    return false;
  }

  virtual bool doesCollide(Eigen::Vector3d& center, double& radius) const {
    return false;
  }

  virtual double pdf_value(const Eigen::Vector3d& origin, const Eigen::Vector3d v) const {return 0;}
  virtual Eigen::Vector3d random(const Eigen::Vector3d& origin) const {return Eigen::Vector3d(1, 0, 0);}
  virtual void reset() {}

  virtual void updatePosition(Eigen::Vector3d move) {}
};

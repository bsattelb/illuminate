#pragma once
#include <eigen3/Eigen/Dense>
#include <algorithm>

#include <iostream>

class BoundingBox {

double fmin(double a, double b) const{
  return a < b ? a : b;
}
double fmax(double a, double b) const{
  return a > b ? a : b;
}

public:
  Eigen::Vector3d min, max;
  BoundingBox() {};
  BoundingBox(Eigen::Vector3d a, Eigen::Vector3d b) {
    min = a;
    max = b;
  }

  bool hit(const Ray& r, double tmin, double tmax) const{
    for(int a = 0; a < 3; a++) {
      double invD = 1.0/r.direction(a);
      double t0 = (min(a) - r.origin(a))*invD;
      double t1 = (max(a) - r.origin(a))*invD;

      if (invD < 0) {
        std::swap(t0, t1);
      }

      tmin = t0 > tmin ? t0 : tmin;
      tmax = t1 < tmax ? t1 : tmax;
      if(tmax <= tmin) {
        return false;
      }
    }
    return true;
  }

  static BoundingBox surrounding_box(BoundingBox box0, BoundingBox box1) {
    Eigen::Vector3d min(0,0,0);
    Eigen::Vector3d max(0,0,0);

    for(int i = 0; i < 3; ++i) {
      min(i) = box0.min(i) < box1.min(i) ? box0.min(i) : box1.min(i);
      max(i) = box0.max(i) > box1.max(i) ? box0.max(i) : box1.max(i);
    }

    return BoundingBox(min, max);
  }
};
